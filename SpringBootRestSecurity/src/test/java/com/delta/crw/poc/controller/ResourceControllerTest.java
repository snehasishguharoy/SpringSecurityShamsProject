/**
 * 
 */
package com.delta.crw.poc.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.delta.crw.poc.config.TestConfig;
import com.delta.crw.poc.repository.UserRepository;
import com.delta.crw.poc.service.GenericService;

/**
 * The Class ResourceControllerTest.
 *
 * @author q17831
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
public class ResourceControllerTest {

	/** The resource controller. */
	@Autowired
	private ResourceController resourceController;

	/** The generic service. */
	@Autowired
	private GenericService genericService;

	/** The user repository. */
	@Autowired
	private UserRepository userRepository;

	/**
	 * Test get all users.
	 */
	@Test
	@WithMockUser(authorities = { "ADMIN_USER" })
	public void testGetAllUsers() {
		resourceController.getAllUsers();
		genericService.findAllUsers();
		Assert.assertNotNull(userRepository.all());

	}

	/**
	 * Testget cities.
	 */
	@Test
	@WithMockUser(authorities = { "ADMIN_USER,STANDARD_USER" })
	public void testgetCities() {
		resourceController.getCities();
		genericService.findAllRandomCities();
		Assert.assertNotNull(userRepository.all());

	}

	/**
	 * Test get cities throw exception.
	 */
	@Test(expected = NullPointerException.class)
	@WithMockUser(authorities = { "ADMIN_USER,STANDARD_USER" })
	public void testGetCities_throwException() {
		ResourceController resourceController = new ResourceController();
		Assert.assertNull(resourceController.getCities());

	}

	/**
	 * Test get all users throw exception.
	 */
	@Test(expected = NullPointerException.class)
	@WithMockUser(authorities = { "ADMIN_USER" })
	public void testGetAllUsers_ThrowException() {
		ResourceController resourceController = new ResourceController();
		Assert.assertNull(resourceController.getAllUsers());

	}

}
