/**
 * 
 */
package com.delta.crw.poc.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.delta.crw.poc.config.TestConfig;
import com.delta.crw.poc.repository.UserRepository;

/**
 * The Class AppUserDetailsServiceTest.
 *
 * @author q17831
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
public class AppUserDetailsServiceTest {

	/** The user repository. */
	@Autowired
	private UserRepository userRepository;

	/**
	 * Load user by username test.
	 */
	@Test
	public void loadUserByUsernameTest() {
		Assert.assertNotNull(userRepository.find("john.doe"));

	}

	/**
	 * Load user by username test throw exception.
	 */
	@Test(expected = NullPointerException.class)
	public void loadUserByUsernameTest_throwException() {
		userRepository = null;
		userRepository.find("john.doe");
	}
}
