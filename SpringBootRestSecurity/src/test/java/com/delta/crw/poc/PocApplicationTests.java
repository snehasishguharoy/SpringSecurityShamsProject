package com.delta.crw.poc;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.delta.crw.poc.controller.ResourceController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PocApplicationTests {
	@Autowired
	private ResourceController resourceController;

	@Test
	public void contextLoads() {
		Assertions.assertThat(resourceController).isNotNull();
	}
	
	


}
