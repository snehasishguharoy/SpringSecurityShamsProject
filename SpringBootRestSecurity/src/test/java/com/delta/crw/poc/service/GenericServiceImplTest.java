/**
 * 
 */
package com.delta.crw.poc.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.delta.crw.poc.config.TestConfig;
import com.delta.crw.poc.repository.RandomCityRepository;
import com.delta.crw.poc.repository.UserRepository;

/**
 * The Class GenericServiceImplTest.
 *
 * @author q17831
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
public class GenericServiceImplTest {

	/** The user repository. */
	@Autowired
	private UserRepository userRepository;

	/** The random city repository. */
	@Autowired
	private RandomCityRepository randomCityRepository;

	/**
	 * Find all users test.
	 */
	@Test
	public void findAllUsersTest() {
		Assert.assertNotNull(userRepository.all());
	}

	/**
	 * Find all users test throw exception.
	 */
	@Test(expected = NullPointerException.class)
	public void findAllUsersTest_throwException() {
		userRepository = null;
		userRepository.all();
	}

	/**
	 * Find all random cities test.
	 */
	@Test
	public void findAllRandomCitiesTest() {
		Assert.assertNotNull(randomCityRepository.all());
	}

	/**
	 * Find all random cities test throw exception.
	 */
	@Test(expected = NullPointerException.class)
	public void findAllRandomCitiesTest_throwException() {
		randomCityRepository = null;
		randomCityRepository.all();
	}

	/**
	 * Find by username test.
	 */
	@Test
	public void findByUsernameTest() {
		Assert.assertNotNull(userRepository.find("john.doe"));
	}

	/**
	 * Find by username test throw exception.
	 */
	@Test(expected = NullPointerException.class)
	public void findByUsernameTest_throwException() {
		userRepository = null;
		userRepository.find("john.doe");
	}

}
