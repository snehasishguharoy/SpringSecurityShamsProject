/**
 * 
 */
package com.delta.crw.poc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.delta.crw.poc.controller.ResourceController;
import com.delta.crw.poc.repository.RandomCityRepository;
import com.delta.crw.poc.repository.UserRepository;
import com.delta.crw.poc.service.AppUserDetailsService;
import com.delta.crw.poc.service.GenericServiceImpl;

/**
 * @author q17831
 *
 */
@Configuration
@ComponentScan(basePackageClasses = { ResourceController.class, AppUserDetailsService.class, GenericServiceImpl.class,
		UserRepository.class, RandomCityRepository.class })
public class TestConfig {

}
