package com.delta.crw.poc.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.delta.crw.poc.domain.Role;
import com.delta.crw.poc.domain.User;
// TODO: Auto-generated Javadoc

/**
 * The Class UserRepository.
 */
@Component
public class UserRepository {
	
	/**
	 * Find.
	 *
	 * @param username the username
	 * @return the user
	 */
	public User find(String username) {
		List<User> users = all();
		User user = null;
		for (User usr : users) {
			if (username.equalsIgnoreCase(usr.getUsername())) {
				user = usr;
				break;
			}
		}
		return user;
	}

	/**
	 * All.
	 *
	 * @return the list
	 */
	public List<User> all() {
		List<User> users = new ArrayList<User>();
		List<Role> roles = null;
		Role role = null;
		
		roles = new ArrayList<Role>();
		User user = new User();
		user.setId(1L);
		user.setFirstName("John");
		user.setLastName("Doe");
		user.setPassword("821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881231a");
		user.setUsername("john.doe");
		role = new Role();
		role.setId(1L);
		role.setRoleName("STANDARD_USER");
		role.setDescription("Standard User - Has no admin rights");
		roles.add(role);
		user.setRoles(roles);
		users.add(user);
		
		roles = new ArrayList<Role>();
		user = new User();
		user.setId(2L);
		user.setFirstName("Admin");
		user.setLastName("Admin");
		user.setPassword("821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881231a");
		user.setUsername("admin.admin");
		role = new Role();
		role.setId(2L);
		role.setRoleName("ADMIN_USER");
		role.setDescription("Admin User - Has permission to perform admin tasks");
		roles.add(role);
		role = new Role();
		role.setId(1L);
		role.setRoleName("STANDARD_USER");
		role.setDescription("Standard User - Has no admin rights");
		roles.add(role);
		user.setRoles(roles);
		users.add(user);
		return users;
	}
}
