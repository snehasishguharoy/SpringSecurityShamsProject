package com.delta.crw.poc.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utility {
	private static final Class<Utility> applicationClass = Utility.class;
	private static final Logger log = LoggerFactory.getLogger(applicationClass);
	
	public static void main(String[] args) throws Exception {
		String sha256hex = org.apache.commons.codec.digest.DigestUtils.sha256Hex("jwtpass");
		log.info(sha256hex);
		
	}
}
