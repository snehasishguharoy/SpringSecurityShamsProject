package com.delta.crw.poc.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.delta.crw.poc.domain.RandomCity;

// TODO: Auto-generated Javadoc
/**
 * The Class RandomCityRepository.
 */
@Component
public class RandomCityRepository {
	
	/**
	 * All.
	 *
	 * @return the list
	 */
	public List<RandomCity> all() {
		List<RandomCity> cities = new ArrayList<RandomCity>();
		RandomCity city = new RandomCity();
		city.setId(1L);
		city.setName("Atlanta");
		cities.add(city);
		city = new RandomCity();
		city.setId(2L);
		city.setName("Mineapolis");
		cities.add(city);
		return cities;
	}
}
