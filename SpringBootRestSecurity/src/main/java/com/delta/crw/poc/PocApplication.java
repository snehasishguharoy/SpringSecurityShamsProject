package com.delta.crw.poc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

// TODO: Auto-generated Javadoc
/**
 * The Class PocApplication.
 */
@SpringBootApplication
public class PocApplication extends SpringBootServletInitializer {
	
	/** The Constant applicationClass. */
	private static final Class<PocApplication> applicationClass = PocApplication.class;
	
	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(applicationClass);

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		log.debug("PocApplication JAR");
		SpringApplication.run(PocApplication.class, args);
	}

	/* (non-Javadoc)
	 * @see org.springframework.boot.web.support.SpringBootServletInitializer#configure(org.springframework.boot.builder.SpringApplicationBuilder)
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		log.debug("PocApplication WAR");
		return application.sources(applicationClass);
	}
}
