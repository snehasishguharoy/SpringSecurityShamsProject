package com.delta.crw.poc.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.delta.crw.poc.domain.RandomCity;
import com.delta.crw.poc.domain.User;
import com.delta.crw.poc.repository.RandomCityRepository;
import com.delta.crw.poc.repository.UserRepository;

// TODO: Auto-generated Javadoc
/**
 * The Class GenericServiceImpl.
 */
@Service
public class GenericServiceImpl implements GenericService {
	
	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(GenericServiceImpl.class);
	
	/** The user repository. */
	@Autowired
	private UserRepository userRepository;
    
    /** The random city repository. */
    @Autowired
    private RandomCityRepository randomCityRepository;

    /* (non-Javadoc)
     * @see com.delta.crw.poc.service.GenericService#findByUsername(java.lang.String)
     */
    @Override
    public User findByUsername(String username) {
    	log.info("#### findByUsername");
        return userRepository.find(username);
    }
    
    /* (non-Javadoc)
     * @see com.delta.crw.poc.service.GenericService#findAllUsers()
     */
    @Override
    public List<User> findAllUsers() {
    	log.info("#### findAllUsers");
        return userRepository.all();
    }

    /* (non-Javadoc)
     * @see com.delta.crw.poc.service.GenericService#findAllRandomCities()
     */
    @Override
    public List<RandomCity> findAllRandomCities() {
        return (List<RandomCity>)randomCityRepository.all();
    }
}