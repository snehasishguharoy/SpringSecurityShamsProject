package com.delta.crw.poc.service;

import com.delta.crw.poc.domain.RandomCity;
import com.delta.crw.poc.domain.User;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface GenericService.
 */
public interface GenericService {
    
    /**
     * Find by username.
     *
     * @param username the username
     * @return the user
     */
    User findByUsername(String username);

    /**
     * Find all users.
     *
     * @return the list
     */
    List<User> findAllUsers();

    /**
     * Find all random cities.
     *
     * @return the list
     */
    List<RandomCity> findAllRandomCities();
}
